package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.DescriptionEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String name, final String description) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        if (StringUtils.isBlank(description)) throw new DescriptionEmptyException();
        return repository.create(name.trim(), description.trim());
    }

    @Override
    public Task create(final String name) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        return repository.create(name.trim());
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        return repository.findAllByProjectId(projectId.trim());
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = findOneById(id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final Task task = findOneById(id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}