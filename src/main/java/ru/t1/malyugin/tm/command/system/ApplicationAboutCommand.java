package ru.t1.malyugin.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String NAME = "about";

    private static final String DESCRIPTION = "Show Author info";

    private static final String ARGUMENT = "-a";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexej Malyugin");
        System.out.println("E-mail: amalyugin@t1-consulting.ru");
    }

}