package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    private static final String NAME = "task-list";

    private static final String DESCRIPTION = "Show task list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.print("ENTER SORT: ");
        System.out.println(Sort.renderValuesList());
        final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        final Sort sort = Sort.getSortByIndex(sortIndex);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTaskList(tasks);
    }

}