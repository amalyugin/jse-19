package ru.t1.malyugin.tm.command.user;

public final class UserRemoveCommand extends AbstractUserCommand {

    private static final String NAME = "user-remove";

    private static final String DESCRIPTION = "remove current user profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE CURRENT USER]");
        final String userId = getAuthService().getUserId();
        getUserService().removeById(userId);
        getAuthService().logout();
    }

}