package ru.t1.malyugin.tm.command.project;

import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-change-status-by-id";

    private static final String DESCRIPTION = "Change project status by id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        final Status status = Status.getStatusByIndex(statusIndex);
        getProjectService().changeProjectStatusById(id, status);
    }

}