package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        return models.stream().filter(user -> StringUtils.equals(login, user.getLogin())).findAny().orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return models.stream().filter(user -> StringUtils.equals(email, user.getEmail())).findAny().orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return models.stream().anyMatch(user -> StringUtils.equals(login, user.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return models.stream().anyMatch(user -> StringUtils.equals(email, user.getEmail()));
    }

    @Override
    public User create(final String login, final String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        return add(user);
    }

    @Override
    public User create(final String login, final String passwordHash, final String email, final Role role) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        user.setRole(role);
        return add(user);
    }

}