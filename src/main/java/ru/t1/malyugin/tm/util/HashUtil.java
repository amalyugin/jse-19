package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    String SECRET = "76534";

    Integer ITERATION = 5455;

    static String salt(final String value) {
        if (StringUtils.isBlank(value)) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        if (StringUtils.isBlank(value)) return null;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] array = messageDigest.digest(value.getBytes());
            final StringBuffer stringBuffer = new StringBuffer();
            for (byte b : array) {
                stringBuffer.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}