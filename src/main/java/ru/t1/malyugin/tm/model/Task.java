package ru.t1.malyugin.tm.model;

import ru.t1.malyugin.tm.api.model.IWBS;
import ru.t1.malyugin.tm.enumerated.Status;

public final class Task extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private String projectId;

    public Task() {
    }

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Task(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = "";
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += String.format("%s: %s IN STATUS '%s'", id, name, status.getDisplayName());
        result += (isDescription ? " -> " + description : "");
        return result;
    }

}