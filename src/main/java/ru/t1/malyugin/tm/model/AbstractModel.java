package ru.t1.malyugin.tm.model;

import java.util.Date;
import java.util.UUID;

public abstract class AbstractModel {

    protected final String id = UUID.randomUUID().toString();

    protected final Date created = new Date();

    public String getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

}