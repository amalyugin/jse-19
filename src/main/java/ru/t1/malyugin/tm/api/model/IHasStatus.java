package ru.t1.malyugin.tm.api.model;

import ru.t1.malyugin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}