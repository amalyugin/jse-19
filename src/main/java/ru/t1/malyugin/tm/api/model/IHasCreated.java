package ru.t1.malyugin.tm.api.model;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

}